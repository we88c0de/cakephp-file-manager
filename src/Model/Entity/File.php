<?php
namespace FileManager\Model\Entity;

use Cake\ORM\Entity;

/**
 * Class MyPlugin
 * @package FileManager\Model\Entity
 */
class File extends Entity
{
    // @codingStandardsIgnoreStart
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    // @codingStandardsIgnoreEnd
}
