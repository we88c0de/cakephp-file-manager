<?php

declare(strict_types=1);

namespace FileManager\Controller\Api;

use \Cake\Controller\Controller;

/**
 * Class AppController
 * @package FileManager\Controller\Api
 */
class AppController extends Controller
{
    /**
     * Initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
    }
}
